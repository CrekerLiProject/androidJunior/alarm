package com.junior.liqiang.junior.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by liqiang on 2018/7/7.
 */

public class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
}