package com.junior.liqiang.junior.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.junior.liqiang.junior.R;
import com.junior.liqiang.junior.pickertime.PickerTimeActivity;

public class MainActivity extends BaseActivity {
    Button ButtonPickerTime;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButtonPickerTime = findViewById(R.id.button_picker_time);
        ButtonPickerTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, PickerTimeActivity.class));
            }
        });
    }


    @Override
    protected void onStart(){
        super.onStart();
    }
    @Override
    protected void onResume(){
        super.onResume();
    }
    @Override
    protected void onPause(){
        super.onPause();
    }
    @Override
    protected void onStop(){
        super.onStop();
    }

}
