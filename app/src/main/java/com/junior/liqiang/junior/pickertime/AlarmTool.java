package com.junior.liqiang.junior.pickertime;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Build;

import com.junior.liqiang.junior.R;

import java.text.SimpleDateFormat;
import java.util.Date;

import static android.content.Context.MODE_PRIVATE;

/**闹钟工具类
 * Created by liqiang on 2018/7/10.
 */

public class AlarmTool {
    MediaPlayer mediaPlayer = new MediaPlayer();
    String getTimeHour(Date date) {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("HH");
        return format.format(date);
    }
    String getTimeMinute(Date date) {
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat format = new SimpleDateFormat("mm");
        return format.format(date);
    }
    String getTimeString(Date date){
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return format.format(date);
    }
    /**
     * 设置一个闹钟
     * @param context 上下文
     * @param alarmTime 设置的时间毫秒值
     */
    public void setAlarm(final Context context, final long alarmTime,int ringEndTime){
        Intent intent=new Intent(context,AutoRingReceived.class);
        intent.setAction( AutoRingReceived.ALARM_RING);
        final AlarmManager alarmManager =(AlarmManager)context.getSystemService( Context.ALARM_SERVICE);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context,0x101,intent,0);
        alarmManager.cancel(pendingIntent);//防止重复设置
        if ( Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT ){
            alarmManager.setExact(
                    AlarmManager.RTC_WAKEUP,//闹钟类型
                    alarmTime,pendingIntent);
        }else{
            alarmManager.set(
                    AlarmManager.RTC_WAKEUP,
                    alarmTime,pendingIntent);
        }
        saveRingTime(context,alarmTime);//保存闹钟
        saveRingEndTime(context,ringEndTime);
    }
    /**
     * 保存闹钟的数据，下次启动app时，方便调用
     * @param ringTime 闹钟时间
     */
    private void saveRingTime(Context context,long ringTime){
        SharedPreferences.Editor editor;
        editor = context.getSharedPreferences("ringTime", MODE_PRIVATE).edit();
        editor.putLong("ringTime", ringTime);
        editor.apply();
    }
    private void saveRingStartTime(Context context,long ringStartTime){
        SharedPreferences.Editor editor;
        editor = context.getSharedPreferences("ringStartTime", MODE_PRIVATE).edit();
        editor.putLong("ringStartTime", ringStartTime);
        editor.apply();
    }
    private void saveRingEndTime(Context context,int ringEndTime){
        SharedPreferences.Editor editor;
        editor = context.getSharedPreferences("ringEndTime", MODE_PRIVATE).edit();
        editor.putInt("ringEndTime", ringEndTime);
        editor.apply();
    }
    /**
     * 读取本地的闹钟毫秒值
     * @return  -1表示读取失败
     */
    public long readRingTime(Context context){
        SharedPreferences pref = context.getSharedPreferences("ringTime", MODE_PRIVATE);
        long ringTime = pref.getLong("ringTime",-1);
        return ringTime;
    }
    public int readRingEndTime(Context context){
        SharedPreferences pref = context.getSharedPreferences("ringEndTime", MODE_PRIVATE);
        int ringEndTime = pref.getInt("ringEndTime",-1);
        return ringEndTime;
    }
    public void playMusic(Context context , int i){
        if(mediaPlayer != null){
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;
        }
        switch(i){
            case 0:
                mediaPlayer = MediaPlayer.create(context, R.raw._1gaobaiqiqiu);
                break;
            case 1:
                mediaPlayer = MediaPlayer.create(context, R.raw._1gaobaiqiqiu);
                break;
            case 2:
                mediaPlayer = MediaPlayer.create(context, R.raw._1gaobaiqiqiu);
                break;
            default:
                mediaPlayer = MediaPlayer.create(context, R.raw._1gaobaiqiqiu);
            break;
        }
        mediaPlayer.setVolume(1f, 1f);
        mediaPlayer.start();
    }
    public void stopMusic(){
        if(mediaPlayer != null){
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }
}
