package com.junior.liqiang.junior.pickertime;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.util.Log;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Random;

/** 接收闹钟的广播
 * Created by liqiang on 2018/7/3.
 */

public class AutoRingReceived extends BroadcastReceiver {
    public final static String ALARM_RING = "com.pxd.alarmandnotice.RING";
    public static final long everyDay = 24 * 3600 * 1000;//每天
    @SuppressLint("ObsoleteSdkInt")
    @Override
    public void onReceive(Context context, Intent intent) {
        if(ALARM_RING.equals(intent.getAction())){
            Log.e("AutoRingReceived","闹钟响了 "+TimeMillisToData(System.currentTimeMillis()));
            AlarmTool alarmTool = new AlarmTool();
            Random random = new Random(  );
            int i =  random.nextInt(3);
            alarmTool.playMusic(context, i );
            AlarmTool alarmTool1 = new AlarmTool();
            alarmTool1.setAlarm( context,getNextTime() ,alarmTool1.readRingEndTime( context ));
        }
    }
    /**
     * 获取下一次闹钟的毫秒值
     * 用于设置重复时间
     * @return 毫秒值
     */
    private long getNextTime() {
        return System.currentTimeMillis() + everyDay;
    }
    /**
     * 将毫秒值转为时间字符串
     * @param timeMillis 毫秒值
     * @return 时间字符串
     */
    private static String TimeMillisToData(final String timeMillis){
        if(timeMillis.equals( "null" )){
            return timeMillis;
        }
        @SuppressLint("SimpleDateFormat") SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis( Long.valueOf(timeMillis));
        return formatter.format(calendar.getTime());
    }
    private static String TimeMillisToData(final String timeMillis,final String format){
        if(timeMillis.equals( "null" )){
            return timeMillis;
        }
        @SuppressLint("SimpleDateFormat") SimpleDateFormat formatter = new SimpleDateFormat(format);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis( Long.valueOf(timeMillis));
        return formatter.format(calendar.getTime());
    }

    /**
     * 将毫秒值转为时间字符串
     * @param timeMillis 毫秒值
     * @return 时间字符串
     */
    public static String TimeMillisToData(final long timeMillis){
        return TimeMillisToData( String.valueOf(timeMillis));
    }
    public static String TimeMillisToData(final long timeMillis,final String format){
        return TimeMillisToData( String.valueOf(timeMillis),format);
    }
}
