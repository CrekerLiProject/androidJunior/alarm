package com.junior.liqiang.junior.pickertime;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.bigkoo.pickerview.TimePickerView;
import com.bigkoo.pickerview.lib.WheelView;
import com.junior.liqiang.junior.BuildConfig;
import com.junior.liqiang.junior.R;
import com.junior.liqiang.junior.activity.BaseActivity;

import java.util.Calendar;
import java.util.Date;
import java.util.Random;
/**闹钟
 * Created by liqiang on 2018/7/8.
 */

public class PickerTimeActivity extends BaseActivity{

    Button ButtonSetAlarm,ButtonTestAlarm;
    TimePickerView pvTime;
    AlarmTool alarmTool = new AlarmTool();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_picker_time);
        ButtonSetAlarm = findViewById(R.id.button_set_my_alarm);
        ButtonTestAlarm = findViewById(R.id.button_test_my_alarm);
        pvTime = new TimePickerView( PickerTimeActivity.this, TimePickerView.Type.HOUR_TO_HOUR );
        pvTime.setTime( new Date() );
        pvTime.setCyclic( true );
        pvTime.setCancelable( true );
        pvTime.setOnTimeSelectListener( new TimePickerView.OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date) {
                final int hour1 = Integer.valueOf(alarmTool.getTimeHour(date));
                final int hour2 = Integer.valueOf(alarmTool.getTimeMinute(date));
                final long alarmTime = getRandomByDate(hour1,hour2);
                alarmTool.setAlarm(PickerTimeActivity.this,alarmTime,hour2);
                if( BuildConfig.TEST_MODE){
                    Toast.makeText(PickerTimeActivity.this,"在 "+AutoRingReceived.TimeMillisToData(alarmTime) + "响铃",Toast.LENGTH_LONG).show();
                }else{
                    Toast.makeText(PickerTimeActivity.this,"在 "+hour1 +"-"+hour2 + "之间响铃",Toast.LENGTH_LONG).show();
                }
            }
        } );

        ButtonSetAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pvTime.show();
            }
        });
        ButtonTestAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Random random = new Random(  );
                int i =  random.nextInt(3);
                alarmTool.playMusic(PickerTimeActivity.this,i);
            }
        });
        //初始化闹钟
        final AlarmTool alarmTool = new AlarmTool();
        final long ringTime = alarmTool.readRingTime(PickerTimeActivity.this);
        final int endTime = alarmTool.readRingEndTime(PickerTimeActivity.this);
        if(ringTime != -1){
            final long alarmTime;
            if(ringTime<System.currentTimeMillis()){
                alarmTime = ringTime + AutoRingReceived.everyDay;
            }else{
                alarmTime = ringTime;
            }
            if( BuildConfig.TEST_MODE){
                Toast.makeText( PickerTimeActivity.this,AutoRingReceived.TimeMillisToData(alarmTime)+"响铃",Toast.LENGTH_SHORT ).show();
            }else{
                int hour1 = Integer.valueOf( AutoRingReceived.TimeMillisToData(alarmTime,"HH"));
                int hour2 = endTime;
                Toast.makeText(PickerTimeActivity.this,"在 "+hour1 +"-"+hour2 + "之间响铃",Toast.LENGTH_LONG).show();
            }
            alarmTool.setAlarm(PickerTimeActivity.this,alarmTime,endTime);
        }else{
            Toast.makeText( PickerTimeActivity.this,"当前没有闹钟，请设置",Toast.LENGTH_SHORT ).show();
        }
    }

    /**
     * 通过日期获得随机日期
     * 返回的数据为两个小时点之间的随机时间点
     * @param hour1 第一个小时点
     * @param hour2 第二个小时点
     * @return
     */
    private long getRandomByDate(int hour1,int hour2){
        final Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY,hour1);
        c.set(Calendar.MINUTE,0);
        long time1 = c.getTimeInMillis();
        c.set(Calendar.HOUR_OF_DAY,hour2);
        c.set(Calendar.MINUTE,0);
        long time2 = c.getTimeInMillis();
        int x = (int)(time2-time1);
        x = x >= 0 ? x:-x;
        final long randomMills = new Random().nextInt( x );
        return randomMills + time1;
    }
    @Override
    protected void onDestroy(){
        super.onDestroy();
        alarmTool.stopMusic();
    }
    @Override
    protected void onStop(){
        super.onStop();
        alarmTool.stopMusic();
    }

}
